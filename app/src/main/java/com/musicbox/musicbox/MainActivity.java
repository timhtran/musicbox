package com.musicbox.musicbox;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jeremyfeinstein.slidingmenu.lib.*;
import com.musicbox.musicbox.tabs.SlidingTabLayout;
import com.perk.perksdk.PerkManager;

import java.io.File;
import java.io.IOException;

public class MainActivity extends NavigationToolbarActivity implements PageFragment.OnTabSelectedListener, NavigationDrawerFragment.OnListItemClickedListener {
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
	private ProgressBar mProgressBar;
	private int mProgress;
	private Handler mHandler = new Handler();
	private static String lastPlayedPath;

	//Playbar stuff
	private MediaPlayer barplayer;
	private ImageView barPlay, barClock, barSkip, barArt;
	private TextView barTitle, barArtist;
	private RelativeLayout playbarLayout;


	@Override
	protected int getLayoutResource() {
		return R.layout.activity_main_appbar;
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

	    PerkManager.startSession(MainActivity.this, API_KEY);
	    //pager for sliding tabs
	    mPager = (ViewPager) findViewById(R.id.pager);
	    mPager.setAdapter(new MusicBoxFragmentAdapter(getSupportFragmentManager(), MainActivity.this));

	    //sliding tabs
	    mTabs = (SlidingTabLayout) findViewById(R.id.main_tabs);
	    mTabs.setDistributeEvenly(true);
	    mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
		    @Override
		    public int getIndicatorColor(int position) {
			    return getResources().getColor(R.color.accentColor);
		    }
	    });
	    mTabs.setViewPager(mPager);

	    //initialize playbar elements
	    mProgressBar = (ProgressBar) findViewById(R.id.play_bar_progress);
	    playbarLayout = (RelativeLayout) findViewById(R.id.playbar);
	    barPlay = (ImageView) findViewById(R.id.playbar_play);
	    barClock = (ImageView) findViewById(R.id.playbar_clock);
	    barSkip = (ImageView) findViewById(R.id.playbar_skip);
	    barTitle = (TextView) findViewById(R.id.playbar_title);
	    barArtist = (TextView) findViewById(R.id.playbar_artist);
	    barArt = (ImageView) findViewById(R.id.playbar_album_art);

	    barplayer = new MediaPlayer();
	    lastPlayedPath = getPreferences(MODE_PRIVATE).getString("lastPlayedTrack", "");
	    try {
		    if (!lastPlayedPath.isEmpty()) {
			    MediaMetadataRetriever metadata = new MediaMetadataRetriever();
			    metadata.setDataSource(lastPlayedPath);
			    String title = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
			    String artist = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);

			    //display
			    barTitle.setText(title);
			    barArtist.setText(artist);
			    byte[] image = metadata.getEmbeddedPicture();
			    barArt.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
			    barPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.play));

			    //get player ready
			    barplayer.reset();
			    barplayer.setDataSource(lastPlayedPath);
			    barplayer.prepare();
			    metadata.release();
		    } else {
			    playbarLayout.setVisibility(View.GONE);
		    }
	    } catch (IOException e) {
		    e.printStackTrace();
	    }

	    //toggle play/pause button
	    barPlay.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
			    if (!barplayer.isPlaying()) {
				    barPlay.setImageResource(R.drawable.pause);
				    String resumeFile = getPreferences(MODE_PRIVATE).getString("lastPlayedTrack", "");
				    startTrack(new File(resumeFile), true);
			    } else {
				    barplayer.pause();
				    barPlay.setImageResource(R.drawable.play);
			    }
		    }
	    });

	    //set listener for playbar swipe action
	    playbarLayout.setOnTouchListener(new View.OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
			    float initialY = 0;
			    switch (event.getAction()) {
				    case MotionEvent.ACTION_DOWN:
					    initialY = event.getY();
					    break;

				    case MotionEvent.ACTION_MOVE:
					    if (initialY > event.getY()) {
					    }
					    break;
			    }

			    return true;
		    }
	    });

//	    //testing FAB
//	    final ImageButton fab_plus = (ImageButton) findViewById(R.id.fab_plus);
//	    fab_plus.setOnClickListener(new View.OnClickListener() {
//		    @Override
//		    public void onClick(View v) {
//			    fab_plus.setSelected(!fab_plus.isSelected());
//			    fab_plus.setImageResource(R.drawable.animated_plus);
//			    Drawable animDrawable = fab_plus.getDrawable();
//			    if (animDrawable instanceof Animatable) {
//				    ((Animatable) animDrawable).start();
//			    }
//		    }
//	    });
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	/*
	 * Methods for talking with fragment
	 */

	/**
	 * Start playing music and update playbar
	 * @param trackFile The file to be played (should be an MP3 file)
	 */
	public void startTrack(File trackFile, boolean resume) {
		if (!resume) {
			//save settings
			SharedPreferences settings = getPreferences(MODE_APPEND);
			settings.edit().putString("lastPlayedTrack", trackFile.getAbsolutePath()).apply();
			playbarLayout.setVisibility(View.VISIBLE);      //TODO: add animation

			MediaMetadataRetriever metadata = new MediaMetadataRetriever();
			metadata.setDataSource(trackFile.getAbsolutePath());
			final String trackTitle = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
			final String trackArtist = metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
			final int duration = Integer.parseInt(metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

			barTitle.setText(trackTitle);       //TODO: scroll text when too long
			barTitle.setHorizontallyScrolling(true);
			barArtist.setText(trackArtist);
			barPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pause));

			byte[] image = metadata.getEmbeddedPicture();
			barArt.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));

			mProgress = 0;
		}

		try {
			//play music
			if (resume) {
				barplayer.start();
			} else {
				barplayer.reset();
				barplayer.setDataSource(trackFile.getAbsolutePath());
				barplayer.prepare();
				barplayer.start();
			}

			mProgressBar.setMax(barplayer.getDuration());

			//start progress bar
			new Thread(new Runnable() {
				@Override
				public void run() {
					while (mProgress < barplayer.getDuration()) {
						try {
							if (barplayer.isPlaying()) {
								Thread.sleep(1100);
								mProgress = barplayer.getCurrentPosition();
								mHandler.post(new Runnable() {
									@Override
									public void run() {
										mProgressBar.setProgress(mProgress);
									}
								});
							}
						} catch (InterruptedException ie) {
							ie.printStackTrace();
						}
					}
				}
			}).start();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Open right fragment and get the correct values for the view
	 * @param songClicked The song that was clicked on to show information
	 */
	@Override
	public void openRightDrawer(File songClicked) {
		//initialize sliding menu
		SlidingMenu rightDrawer = new SlidingMenu(this);
		rightDrawer.setMode(SlidingMenu.RIGHT);
		rightDrawer.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);          //disable swiping
		rightDrawer.setBehindOffset(SharedValue.dp2px(123, this));
		rightDrawer.setFadeDegree(0.35f);
		rightDrawer.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		rightDrawer.setMenu(R.layout.fragment_right_drawer);
		View slideMenuRoot = rightDrawer.getRootView();

		//initiate displayed components
		ImageView albumArt = (ImageView) slideMenuRoot.findViewById(R.id.right_drawer_album_art);
		TextView albumDate = (TextView) slideMenuRoot.findViewById(R.id.right_drawer_album_date);
		TextView songTitle = (TextView) slideMenuRoot.findViewById(R.id.right_drawer_song_title);
		TextView songArtist = (TextView) slideMenuRoot.findViewById(R.id.right_drawer_song_artist);
		TextView songSeektime = (TextView) slideMenuRoot.findViewById(R.id.right_drawer_seek_time);

		//retrieve data and set display
		MediaMetadataRetriever metadata = new MediaMetadataRetriever();
		metadata.setDataSource(songClicked.getAbsolutePath());

		byte[] image = metadata.getEmbeddedPicture();
		albumArt.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));
		albumDate.setText(metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));
		songTitle.setText(metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
		songArtist.setText(metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));

		//convert duration from millis to minutes and seconds
		int songDuration = Integer.parseInt(metadata.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
		int min = songDuration/60000;
		double sec = (songDuration / 60000.00 - min) * 60.00;
		String duration = min + ":" + (sec < 10 ? "0" : "") + Math.round(sec);
		songSeektime.setText(duration);

		rightDrawer.showMenu(true);
		metadata.release();
	}

	/**
	 * Start or stop floating widget service
	 */
	@Override
	public void setWidgetEnabled(boolean enabled) {
		Intent widgetService = new Intent(this, FloatingWidgetService.class);
		if (enabled)
			startService(widgetService);
		else
			stopService(widgetService);
	}

	@Override
	public void showPerkPortal() {
		PerkManager.showPortal(this, API_KEY);
	}

	@Override
	public void perkEventTriggered() {
		PerkManager.trackEvent(MainActivity.this, API_KEY, EVENT_ID, false, null, false);
	}

	@Override
	public void openPreference() {
		startActivity(new Intent(MainActivity.this, SettingsActivity.class));
	}
}
