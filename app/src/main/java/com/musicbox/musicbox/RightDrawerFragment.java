package com.musicbox.musicbox;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

/**
 * A simple {@link Fragment} subclass.
 */
public class RightDrawerFragment extends Fragment {

    private DrawerLayout mDrawerLayout;
    private View navView;

    public RightDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_right_drawer, container, false);
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, android.support.v7.widget.Toolbar toolbar) {
        navView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
    }
}
