package com.musicbox.musicbox;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.perk.perksdk.PerkManager;

public abstract class NavigationToolbarActivity extends AppCompatActivity {
	protected Toolbar mToolbar;
	protected DrawerLayout mDrawerLayout;

	protected String API_KEY = "950b0535f07fb017f3d1f120fdd648df97d8dea0";
	protected String EVENT_ID = "b4c061f2fcaf7cfcdd4945ab079d3360b3ce624f";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResource());

		mToolbar = (Toolbar) findViewById(R.id.app_bar);

		//set up nav drawer fragment
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		NavigationDrawerFragment navDrawer = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
		navDrawer.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);

		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

	}

	protected abstract int getLayoutResource();
}
