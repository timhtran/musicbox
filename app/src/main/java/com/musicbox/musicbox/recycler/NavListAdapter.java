package com.musicbox.musicbox.recycler;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.musicbox.musicbox.MainActivity;
import com.musicbox.musicbox.R;
import com.musicbox.musicbox.SettingsActivity;

import java.util.Collections;
import java.util.List;

public class NavListAdapter extends RecyclerView.Adapter<NavListAdapter.ViewHolder> {
	private LayoutInflater mInflater;
	private List<NavListRow> mListRows;

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public ImageView icon;
		public TextView title;

		public ViewHolder(View containerLayout) {
			super(containerLayout);
			icon = (ImageView) containerLayout.findViewById(R.id.nav_item_icon);
			title = (TextView) containerLayout.findViewById(R.id.nav_item_title);
		}
	}

	public NavListAdapter(Context context, List<NavListRow> data) {
		mInflater = LayoutInflater.from(context);
		mListRows = data;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		final View view = mInflater.inflate(R.layout.recycler_list_item, parent, false);
		ViewHolder vh = new ViewHolder(view);
		return vh;
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		NavListRow currentRow = mListRows.get(position);
		holder.icon.setImageResource(currentRow.iconId);
		holder.title.setText(currentRow.rowText);
	}

	@Override
	public int getItemCount() {
		return mListRows.size();
	}
}
